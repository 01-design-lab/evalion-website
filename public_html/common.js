menuItems = document.querySelectorAll("menu>li>a")
menuItems.forEach(el => {
  if(el.nextElementSibling && el.nextElementSibling.tagName == "NAV"){
    el.addEventListener("click", function(e){
      menuItems.forEach(el => {
        el.parentNode.classList.remove("active")
      })
      this.parentNode.classList.add("active")
      document.querySelector('header').classList.add("openMenu")
      e.preventDefault()
    })
  }
})
document.getElementById("x").addEventListener("click", function(e){
  document.querySelectorAll("menu li.active").forEach(el => {
    el.classList.remove("active")
    // this is inside here because the above is only expected to happen once
    // and if it doesn't the following shouldn't happen either. No need for an
    // extra if.
    document.querySelector(".openMenu").classList.remove("openMenu")
  })
})